// // // Get DOM elements
// const closeSidebarButton = document.querySelector('.close-sidebar');
// const newChatButton = document.querySelector('.new-chat');
// const listOfChats = document.querySelector('.list-of-chats');
// const leftPane = document.querySelector('.left-pane');

// // Function to toggle the left pane visibility
// function toggleLeftPane() {
//     leftPane.classList.toggle('hidden');
// }

// // Function to add a new chat item
// function addNewChat() {
//     const chatItem = document.createElement('div');
//     chatItem.classList.add('single-chat');
//     chatItem.innerHTML = `
//         <div class="chat-icon"></div>
//         <div class="chat-title">New Chat</div>
//     `;
//     listOfChats.appendChild(chatItem);
// }

// // Event listeners
// closeSidebarButton.addEventListener('click', toggleLeftPane);
// newChatButton.addEventListener('click', addNewChat);
// Get DOM elements
const closeSidebarButton = document.querySelector('.close-sidebar');
const newChatButton = document.querySelector('.new-chat');
const listOfChats = document.querySelector('.list-of-chats');
const leftPane = document.querySelector('.left-pane');

// Function to toggle the left pane visibility
function toggleLeftPane() {
    leftPane.classList.toggle('hidden');
}

// Function to add a new chat item
function addNewChat() {
    const chatItem = document.createElement('div');
    chatItem.classList.add('single-chat');
    chatItem.innerHTML = `
        <div class="chat-icon"></div>
        <div class="chat-title">New Chat</div>
    `;
    listOfChats.appendChild(chatItem);
}

// Event listeners
closeSidebarButton.addEventListener('click', toggleLeftPane);
newChatButton.addEventListener('click', addNewChat);

// Get DOM elements
// const closeSidebarButton = document.querySelector('.close-sidebar');
// const showSidebarButton = document.querySelector('.show-sidebar');
// const newChatButton = document.querySelector('.new-chat');
// const listOfChats = document.querySelector('.list-of-chats');
// const leftPane = document.querySelector('.left-pane');

// // Function to toggle the left pane visibility and buttons
// function toggleLeftPane() {
//     leftPane.classList.toggle('hidden');
//     closeSidebarButton.classList.toggle('hidden');
//     showSidebarButton.classList.toggle('hidden');
// }

// // Function to add a new chat item
// function addNewChat() {
//     const chatItem = document.createElement('div');
//     chatItem.classList.add('single-chat');
//     chatItem.innerHTML = `
//         <div class="chat-icon"></div>
//         <div class="chat-title">New Chat</div>
//     `;
//     listOfChats.appendChild(chatItem);
// }

// Event listeners
closeSidebarButton.addEventListener('click', toggleLeftPane);
showSidebarButton.addEventListener('click', toggleLeftPane);
newChatButton.addEventListener('click', addNewChat);

// Initially hide the close_sidebar button and show_sidebar button
closeSidebarButton.classList.add('hidden');
showSidebarButton.classList.remove('hidden');
// ... (previous JavaScript) ...

const showSidebarButton = document.querySelector('.show-sidebar');

// ... (previous JavaScript) ...

// Initially hide the close_sidebar button and show_sidebar button
closeSidebarButton.classList.add('hidden');
showSidebarButton.classList.remove('hidden');


addMessage('Welcome to the chat!', false);

// Get DOM elements
const singleChatItems = document.querySelectorAll('.single-chat');
const messagesSection = document.querySelector('.messages-section');

// Function to simulate loading messages for a chat
function loadChatMessages(chatTitle) {
    // In a real application, you would load messages for the selected chat here.
    // For now, let's simulate it with a message.
    messagesSection.innerHTML = `<div class="message-card sent">${chatTitle}</div>`;
}

// Add click event listeners to single chat items
singleChatItems.forEach((chatItem) => {
    chatItem.addEventListener('click', () => {
        // Load messages for the selected chat
        const chatTitle = chatItem.querySelector('.chat-title').textContent;
        loadChatMessages(chatTitle);
    });
});

// Get DOM elements
const gpt3Button = document.getElementById('gpt-3');
const gpt4Button = document.getElementById('gpt-4');

// Function to toggle the active state of the buttons
function toggleActiveButton(button) {
    button.classList.toggle('active');
}

// Event listeners for the GPT buttons
gpt3Button.addEventListener('click', () => {
    toggleActiveButton(gpt3Button);
    toggleActiveButton(gpt4Button);
});

gpt4Button.addEventListener('click', () => {
    toggleActiveButton(gpt4Button);
    toggleActiveButton(gpt3Button);
});

// Get DOM elements
// const sendIcon = document.querySelector('.send-icon');
// const inputBox = document.querySelector('.input-box');
// const messagesSection = document.querySelector('.messages-section');

// // Function to add a new message
// function addMessage(text, isSent) {
//     const messageCard = document.createElement('div');
//     messageCard.classList.add('message-card');
//     if (isSent) {
//         messageCard.classList.add('sent');
//     }
//     messageCard.textContent = text;
//     messagesSection.appendChild(messageCard);
//     // Scroll to the bottom of the messages section
//     messagesSection.scrollTop = messagesSection.scrollHeight;
// }

// // Event listener for sending a message
// sendIcon.addEventListener('click', () => {
//     const messageText = inputBox.value.trim();
//     if (messageText) {
//         addMessage(messageText, true);
//         inputBox.value = ''; // Clear the input
//         // In a real application, you would send the message to the server here.
//         // For now, let's simulate a reply from the other user.
//         setTimeout(() => {
//             addMessage('This is a reply from the other user.', false);
//         }, 1000);
//     }
// });


